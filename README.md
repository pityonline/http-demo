# http-demo

Quick setup for http-demo development environment and CI/CD pipelines.

The code sample is from [learn-go-with-tests](https://github.com/quii/learn-go-with-tests/tree/main/http-server/v1), some trivial changes have been made.

## Prerequisites

* a Debian 10 Linux host with [docker](https://docs.docker.com/engine/install/debian/) and [git](https://git-scm.com/download/linux) installed
* good connection to internet
* a free account of https://gitlab.com and https://heroku.com
* ability to push code to gitlab and heroku, this means the authentication is already setup

## Usage

Edit `docker/env_init.sh` files to setup the environment. Then run commands on your host machine to build the docker image for development environment. This image has golang, heroku, git, vim (with several useful plugins) installed.

**TIP**: The docker image building will take about 8-15 minutes to finish, depends on your network connection. After you setup `docker/env_init.sh`, you can run it to initialize a simple environment without golang and heroku (but `git` is needed at least), and then skip to **Continuous Integration** to use gitlab.com to finish the deployment.

```
source .alias
docker build -t http-demo-dev:latest .
init-demo   # init environment
run-demo    # run the application in a docker container
enter-demo  # enter the devlopment docker container
```

In the docker container, then you can modify the files and commit your changes, and push to gitlab and trigger the CI/CD pipelines.

```
vi main.go  # change something
git add main.go
git commit -m 'your commit message'
git push origin master
```

Or you can deploy to heroku manually.

```
git push heroku master
```

## Continuous Integration

Use Gitlab CI for CI pipelines.

### CI jobs

* test
    * lint
    * unit-test
    * build
* env
    * env

## Continuous Delivery

Use dpl for CD. For security and automation reasons, some variables should be added into Settings->CI/CD->Variables in the Gitlab repository.

These settings should be done in `docker/env_init.sh`.

* Key: `HEROKU_API_KEY` Value: **SECRET** Masked: `True`
* Key: `HEROKU_APP_PRODUCTION` Value: `http-demo-prod` Protected: `True`
* Key: `HEROKU_APP_STAGING` Value: `http-demo-staging`
* Key: `REG_TOKEN` Value: **SECRET** Masked: `True`

### CD jobs

* deploy
    * staging
    * production

## Further Thoughts

### High Availability & Scalability

You can use [nginx](https://nginx.org/) or [HAProxy](http://www.haproxy.org/) as a load balancer to make this app high available in Azure, Aliyun. See heroku's document (paid version): [Scale the app](https://devcenter.heroku.com/articles/getting-started-with-go#scale-the-app).

### Monitoring & Alerting

Heroku offers [Monitor solutions](https://devcenter.heroku.com/articles/metrics) for paid plans. Some alternatives could be: [Prometheus](https://prometheus.io/), [Netdata](https://netdata.cloud/), [Open-Falcon](https://open-falcon.org/) and [New Relic](https://newrelic.com/), etc. Some of them offers container level monitoring.

### Logging & Analytics

You can use [ELK](https://www.elastic.co/what-is/elk-stack) for log collection and analytics. Heroku also has a [Logging](https://devcenter.heroku.com/articles/logging) solution.

## References

* [Deploy docker to heroku](https://devcenter.heroku.com/articles/container-registry-and-runtime#using-a-ci-cd-platform)
* [Deploy with terraform](https://devcenter.heroku.com/articles/using-terraform-with-heroku)
