FROM golang:latest

# Settings

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

COPY docker .

# export image environment variables
ENV TZ=Asia/Shanghai

RUN ./build.sh

EXPOSE 5000

ENTRYPOINT ["/go/bin/http-demo"]
