package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	handler := http.HandlerFunc(PlayerServer)
	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "5000"
	}
	fmt.Println("Starting server on port " + PORT)

	if err := http.ListenAndServe(":"+PORT, handler); err != nil {
		log.Fatalf("could not listen on port %v %v", PORT, err)
	}

}
