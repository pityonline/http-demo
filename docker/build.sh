#!/bin/sh

set -e

# Use aliyun mirror instead of official mirror
mv ./buster.aliyun.list /etc/apt/sources.list \
    && mv .gitconfig .tmux.conf .vimrc $HOME

# Install developers tools
apt-get update && \
apt-get install -y -qq \
    less \
    sudo \
    tmux \
    vim

# Install heroku cli
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh

# Set go env
go env -w GO111MODULE=on
go env -w  GOPROXY=https://goproxy.cn

# Build code
git clone https://gitlab.com/pityonline/http-demo /go/src/gitlab.com/pityonline/http-demo
cd /go/src/gitlab.com/pityonline/http-demo
make test && make install

# vim tools
CLONE='git clone --depth 1'
vim_plug_path="$HOME/.vim/pack/plugins/start"
$CLONE https://github.com/fatih/vim-go $vim_plug_path/vim-go
$CLONE https://github.com/preservim/nerdtree $vim_plug_path/nerdtree
$CLONE https://github.com/vim-syntastic/syntastic $vim_plug_path/syntastic
vim -c "GoInstallBinaries" -c ":q"

# Clean up
cd /go
apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -- "$0"
