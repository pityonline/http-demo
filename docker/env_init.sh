#!/usr/bin/env bash

# Before you run this script, please finish authentication of gitlab.com and heroku.com and specify the values.

# Go to https://dashboard.heroku.com/account, in API Key area, click Reveal, copy the value
export HEROKU_API_KEY=

# Go to https://gitlab.com/profile/personal_access_tokens and fill the values
# Name: registry Scopes: read_registry, write_registry
# then click Creat personal token, copy the value
export REG_TOKEN=

# Go to https://gitlab.com/profile/personal_access_tokens, fill the values
# Name: api, Scopes: api, read_api, read_repository, write_repository
# then click Creat personal token, copy the value
GITLAB_PRIVATE_TOKEN=

# On the project page of gitlab.com, go to Settings->Naming, topics, avatar->Project ID, copy the value
project_id=

# Project names, please replace them with unique names, e.g. http-demo-324-prod, http-demo-324-staging
export HEROKU_APP_PRODUCTION=http-demo-prod
export HEROKU_APP_STAGING=http-demo-staging

ssh -T git@gitlab.com
heroku auth:whoami

heroku create $HEROKU_APP_PRODUCTION --buildpack https://github.com/heroku/heroku-buildpack-go.git
heroku create $HEROKU_APP_STAGING --buildpack https://github.com/heroku/heroku-buildpack-go.git

# Project-level Variables API document: https://docs.gitlab.com/ce/api/project_level_variables.html
var_api_url="https://gitlab.com/api/v4/projects/$project_id/variables"

curl --request POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" $var_api_url --form "key=HEROKU_APP_PRODUCTION" --form "value=$HEROKU_APP_PRODUCTION" --form "protected=true"
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" $var_api_url --form "key=HEROKU_APP_STAGING" --form "value=$HEROKU_APP_STAGING"
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" $var_api_url --form "key=HEROKU_API_KEY" --form "value=$HEROKU_API_KEY" --form "masked=true"
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" $var_api_url --form "key=REG_TOKEN" --form "value=$REG_TOKEN" --form "masked=true"
